# ics-ans-demo

Ansible playbook for demo.

## Requirements

- ansible >= 2.7
- molecule >= 2.19

## License

BSD 2-clause
